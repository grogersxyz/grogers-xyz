# [My Personal Website](https://grogers.xyz)

## TODO

- [X] Add Hugo Site
- [X] Add Hugo Coder Theme
- [X] Customize default site
- [X] Add Simple Hosting (for now GitLab pages)
- [X] Set up a gitlab-runner for building the website
- [X] Create a gitlab pipeline
- [ ] Write some tests for the website
- [ ] Switch to Route53 and Let's Encrypt